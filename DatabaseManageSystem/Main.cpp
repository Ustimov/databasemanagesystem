#include "Table.h"

int main()
{
	std::string tableName = "Students";
	std::string workingDir = "C:\\Users\\Ustimov\\Documents\\visual studio 2015\\Projects\\DatabaseManageSystem\\Resources\\";


	Table students(tableName, workingDir);
	students.ReadTable();
	students.PrintTable();
	return 0;
}
