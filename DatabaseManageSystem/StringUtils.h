#pragma once

#include <vector>

class StringUtils {
public:
	static void Split(const std::string & line, const char & delimiter, std::vector<std::string> & parts);
};

