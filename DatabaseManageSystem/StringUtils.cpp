#include "StringUtils.h"

#include <vector>
#include <string>
#include <sstream>

void StringUtils::Split(const std::string & line, const char & delimiter, std::vector<std::string> & parts) {
	std::stringstream ss;
	ss.str(line);
	std::string part;
	while (getline(ss, part, delimiter)) {
		parts.push_back(part);
	}
}
