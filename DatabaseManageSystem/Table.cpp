#include "Table.h"

#include <iostream>
#include <fstream>
#include <string>
#include "StringUtils.h"

Table::Table(const std::string & tableName, const std::string & workingDir) {
	this->tableName = tableName;
	this->workingDir = workingDir;
	this->tableFilePath = workingDir + tableName + ".csv";
}

void Table::ReadTable() {
	std::ifstream in(tableFilePath);
	if (in.is_open()) {
		ParseCsvFile(in);
	}
	else {
		std::cout << "Table " << tableName << " not found" << std::endl;
	}
}

void Table::ParseHeader(const std::string & header) {
	std::vector<std::string> parts;
	StringUtils::Split(header, '|', parts);
	std::vector<std::string> nameAndType;
	for (std::vector<std::string>::iterator it = parts.begin(); it != parts.end(); ++it) {
		nameAndType.clear();
		StringUtils::Split(*it, ',', nameAndType);
		newHeader[nameAndType[0]] = nameAndType[1];
	}
}

void * Table::GetValue(const std::string & stringValue, const std::string & type) {
	// Todo
	return (void *)stringValue.c_str();
}

std::string Table::ToString(void * value, const std::string & type) {
	// Todo
	return std::string((char *)value);
}

void Table::ParseRow(const std::string & stringRow) {
	std::vector<std::string> parts;
	StringUtils::Split(stringRow, '|', parts);
	std::map<std::string, void *> row;
	for (std::map<std::string, std::string>::iterator it = newHeader.begin(); it != newHeader.end(); ++it) {
		row[it->first] = GetValue(parts[std::distance(newHeader.begin(), it)], it->second);
	}
	newRows.push_back(row);
}

void Table::ParseCsvFile(std::ifstream & in) {
	std::string line;
	if (!in.eof()) {
		getline(in, line);
		ParseHeader(line);
	}
	while (!in.eof()) {
		std::cout << line << std::endl;
		getline(in, line);
		ParseRow(line);
	}
}

void Table::PrintTable() {
	for (std::map<std::string, std::string>::iterator it = newHeader.begin(); it != newHeader.end(); ++it) {
		std::cout << it->first;
		if (it != newHeader.end()) {
			std::cout << "|";
		}
	}
	std::cout << std::endl;
	for (std::vector<std::map<std::string, void *>>::iterator rowIt = newRows.begin(); rowIt != newRows.end(); ++rowIt) {
		for (std::map<std::string, std::string>::iterator headerIt = newHeader.begin(); headerIt != newHeader.end(); ++headerIt) {
			std::cout << ToString((*rowIt)[headerIt->first], headerIt->second);
			if (headerIt != newHeader.end()) {
				std::cout << "|";
			}
		}
		std::cout << std::endl;
	}
}

void Table::WriteTable() {
	// Todo
	std::ofstream out(tableFilePath);
	if (!out.is_open()) {
		std::cout << "Can't open file" << std::endl;
	}

	for (int i = 0; i < header.size(); i++) {
		out << header[i].first << "," << header[i].second;
		if (i != header.size() - 1) {
			out << "|";
		}
	}

	out << std::endl;

	for (int i = 0; i < rows.size(); i++) {
		for (int j = 0; j < header.size(); j++) {
			out << rows[i][header[j].first];
			if (j != header.size() - 1) {
				out << "|";
			}
		}
		out << std::endl;
	}

	out << std::endl;
}