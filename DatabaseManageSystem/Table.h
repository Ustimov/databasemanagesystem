#pragma once

#include <vector>
#include <map>

typedef std::map<std::string, std::string> Row;
typedef std::vector<std::pair<std::string, std::string>> Header;

class Table {
public:
	Table(const std::string & tableName, const std::string & workingDir);
	void ReadTable();
	void WriteTable();
	void PrintTable();
private:
	std::string tableName;
	std::string workingDir;
	std::string tableFilePath;
	Header header;
	std::map<std::string, std::string> newHeader;
	std::vector<Row> rows;
	std::vector<std::map<std::string, void *>> newRows;
	void ParseHeader(const std::string & header);
	void * GetValue(const std::string & stringValue, const std::string & type);
	std::string ToString(void * value, const std::string & type);
	void ParseRow(const std::string & row);
	void ParseCsvFile(std::ifstream & in);
};
